#!/bin/bash

echo What is the Inventory Host Group name for this Coredump workaround deployment?

read task

sudo ansible-playbook -i hostlist -k cohesity_coredump_workaround.yml -e "grp=$task"
