#!/bin/bash

a=''

case $@ in
$a)

printf 'Please provide the inventory file after the script.\nFor example: ./cohesity_wrapper.sh [inventory file]\n'

exit 1

;;

esac

echo "Please enter the Cluster VIP for cohesity"

read cluster_vip

echo "Please enter the user name of the admin"

read user

echo "Please enter the password of user"

read -s pass

echo "Attempt Number of trying to run on these set of hosts"

read attempt

for host in $(cat $1); do ansible-playbook cohesity_lastsnapshot_audit.yml -e cohesity_server=$cluster_vip -e cohesity_admin=$user -e cohesity_password=$pass -e host=$host; done | grep -A 10 "Host Summary of all Parameters" >> cohesity_patching_audit_$(date +%F)_${cluster_vip}_${user}_attempt${attempt}
