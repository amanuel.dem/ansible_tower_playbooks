#!/bin/bash

token=$(curl -ksX POST \
https://s-s1-cohesity-1.uspto.gov/irisservices/api/v1/public/accessTokens \
-H 'Content-Type: application/json' \
-d	' {
	"password": "Ballers_phenom30!",
	"username": "ademissew",
	"domain": "uspto.gov"
	} ')


authtoken=$(echo $token | jq -r '.accessToken')

curl -ksX GET https://s-s1-cohesity-1.uspto.gov/irisservices/api/v1/public/protectionSources/objects -H "Authorization: Bearer $authtoken" -H 'Accept: application/json' -H 'Content-type: application/json' | jq -r '.[]?.physicalProtectionSource?.agents[]? | select( .version == "6.5.1d_release-20201226_1fee0cd5" ) | .name' | tr [:upper:] [:lower:] | uniq -u | sort > cohesity_s1_hosts_$(date +%F).txt

total_s1=$(cat cohesity_s1_hosts_$(date +%F).txt | wc -l)

echo "Attached is the report for the S1 Cluster" | mailx -r root@ademissew-ansibletower-test.pt.uspto.gov -s "Total number of S1 Hosts with Cohesity 6.5 agent:  $total_s1" -a cohesity_s1_hosts_$(date +%F).txt king.coleman@uspto.gov,osos_unix@uspto.gov,fahd.husain@uspto.gov,joshua.dike@uspto.gov,david.beltran@uspto.gov,amanuel.demissew@uspto.gov

token2=$(curl -ksX POST \
https://s-s2-cohesity-1.uspto.gov/irisservices/api/v1/public/accessTokens \
-H 'Content-Type: application/json' \
-d      ' {
        "password": "Ballers_phenom30!",
        "username": "ademissew",
        "domain": "uspto.gov"
        } ')

authtoken2=$(echo $token2 | jq -r '.accessToken')

curl -ksX GET https://s-s2-cohesity-1.uspto.gov/irisservices/api/v1/public/protectionSources/objects -H "Authorization: Bearer $authtoken2" -H 'Accept: application/json' -H 'Content-type: application/json' | jq -r '.[]?.physicalProtectionSource?.agents[]? | select( .version == "6.5.1d_release-20201226_1fee0cd5" ) | .name' | tr [:upper:] [:lower:] | uniq -u | sort > cohesity_s2_hosts_$(date +%F).txt

total_s2=$(cat cohesity_s2_hosts_$(date +%F).txt | wc -l)

echo "Attached is the report for the S2 Cluster" | mailx -r root@ademissew-ansibletower-test.pt.uspto.gov -s "Total number of S2 Hosts with Cohesity 6.5 agent:  $total_s2" -a cohesity_s2_hosts_$(date +%F).txt king.coleman@uspto.gov,osos_unix@uspto.gov,fahd.husain@uspto.gov,joshua.dike@uspto.gov,david.beltran@uspto.gov,amanuel.demissew@uspto.gov
