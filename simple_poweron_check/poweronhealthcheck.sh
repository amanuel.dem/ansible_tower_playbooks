#!/bin/bash

echo "**What is the inventory file that you want to check?** (Relative Path or Full Path accepted)"

read inv_file

echo "**What is the inventory subgroup to run it on?** (Specify group or simply put all if you want run on entire inventory)"

read group

ansible-playbook -i $inv_file poweron_healthcheck.yml -k -u root -e grp=$group
